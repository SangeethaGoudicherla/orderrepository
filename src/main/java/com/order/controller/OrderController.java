package com.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/order")
public class OrderController {

	@Autowired
	Environment environment;
	
	@GetMapping("/success")
	public String getMessage() {
		return "Success from order service";
	}
	
	@GetMapping("/port")
	public String getPort() {
		return "Port Number of order service:"+environment.getProperty("local.server.port");
	}
}
